(function () {
    'use strict';

    angular.module('app', [
        'ngRoute',
        'ngDialog',
        'app.api',
        'app.index',
        'ngMaterial'
    ])
        .config(config);

    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        active();

        function active() {
            $routeProvider.when('/confirm', {
                template: '',
                controller: 'ConfirmCtrl'
            });
            $routeProvider.otherwise("/");
        }
    }
})();
