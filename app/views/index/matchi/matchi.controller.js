(function () {
    'use strict';

    angular.module('app.index').controller('MatchiCtrl', MatchiCtrl);

    MatchiCtrl.$inject = ['Matchii','ngDialog','Auth'];

    function MatchiCtrl(Matchii, ngDialog, Auth) {
        var vm = this;
        vm.openLogin = openLogin;
        vm.Auth = Auth;

        activate();

        function activate() {
            var matchii = Matchii.query();
            vm.matchii = matchii;
        }

        function openLogin() {
            ngDialog.open({
                template: 'views/index/pop-up/pop-up.html',
                showClose: false,
                className: 'ngdialog-theme-default pop-modal-dialog-content'
            });
        }

    }
})();