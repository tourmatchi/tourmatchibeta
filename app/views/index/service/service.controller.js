(function () {
    'use strict';

    angular.module('app.index').controller('ServiceCtrl', ServiceCtrl);

    ServiceCtrl.$inject = ['Services'];

    function ServiceCtrl(Services) {
        var vm = this;

        activate();

        function activate() {
            var services = Services.query();
            vm.services = services;
        }
    }
})();