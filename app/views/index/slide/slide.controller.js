(function () {
    'use strict';

    angular.module('app.index').controller('SlideCtrl', SlideCtrl);

    SlideCtrl.$inject = ['Slides', 'ngDialog', '$location', '$anchorScroll', 'Auth', '$window', 'Accounts'];

    function SlideCtrl(Slides, ngDialog, $location, $anchorScroll, Auth, $window, Accounts) {
        var vm = this;
        vm.openLogin = openLogin;
        vm.scrollTo = scrollTo;
        vm.logout = logout;
        vm.Auth = Auth;

        activate();

        function activate() {
            var slides = Slides.query();
            vm.slides = slides;
        }

        function openLogin() {
            ngDialog.open({
                template: 'views/index/pop-up/pop-up.html',
                showClose: false,
                className: 'ngdialog-theme-default pop-modal-dialog-content'
            });
        }


        function logout() {
            Accounts.logout();
            alert("登出！");
            $window.location.reload();
        }

        function scrollTo(id) {
            var hash = $location.hash();
            $location.hash(id);
            console.log($location.hash());
            $anchorScroll();
            $location.hash(hash);
        }
    }
})();