(function () {
    'use strict';

    angular.module('app.index').controller("UserRequestFormController", UserRequestFormController);

    UserRequestFormController.$inject = ['UserForms', 'Auth', '$window', 'Accounts'];

    function UserRequestFormController(UserForms, Auth, $window, Accounts) {
        var vm = this;
        vm.name = Auth.name;
        vm.email = Auth.email;
        vm.Auth = Auth;
        vm.userForm = {
            age0_12: 0,
            age13_18: 0,
            age19_35: 0,
            age36_50: 0,
            age51_65: 0,
            age65up: 0,
            topics: []
        };
        vm.userFormForm = {};
        vm.submitEnable = true;
        vm.toggleTopicSelected = toggleTopicSelected;

        activate();

        function activate() {
            var topics = UserForms.topics.query();
            vm.topics = topics;
            vm.submit = submit;
            vm.cities = [
                "倫敦",
                "洛杉磯",
                "阿姆斯特丹",
                "巴黎",
                "馬德里",
                "美國西部 ",
                "首爾",
                "日本富山",
                "布達佩斯",
                "義大利",
                "柏林",
                "布里斯本",
                "京都",
                "紐約",
                "東京"];
        }

        function toggleTopicSelected(topic) {
            var idx = vm.userForm.topics.indexOf(topic);
            // is currently selected
            if (idx > -1) {
                vm.userForm.topics.splice(idx, 1);
            }
            // is newly selected
            else {
                vm.userForm.topics.push(topic);
            }
        }

        function submit() {
            if (vm.userFormForm.$valid) {
                vm.submitEnable = false;
                var date = vm.selectedDate;
                vm.userForm.date = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
                UserForms.postUserForm(vm.userForm, function (data) {
                    console.log(data);
                    alert('我們已經收到您的表單囉！請靜候3日到您的信箱收取專屬的一日行程表');
                    vm.submitEnable = true;
                    window.location.replace("http://www.tourmatchi.com");
                }, function (error) {
                    console.log(error);
                    alert('請先登入！');
                    Accounts.logout();
                    $window.location.reload();
                    Auth.clean();
                    vm.submitEnable = true;
                });
            } else {
                console.log(vm.userForm.$error);
                alert('請填完必填欄位！');
            }
        }
    }
})();