/**
 * Created by user on 3/18/2016.
 */
(function () {
    'use strict';

    angular.module('app.index').controller('TryCtrl', TryCtrl);

    TryCtrl.$inject = ['Matchii','ngDialog'];

    function TryCtrl(Matchii, ngDialog) {
        var vm = this;
        vm.openLogin = openLogin;

        activate();

        function activate() {
            var matchii = Matchii.query();
            vm.matchii = matchii;
        }

        function openLogin() {
            ngDialog.open({
                template: 'views/index/pop-up/pop-up.html',
                showClose: false,
                className: 'ngdialog-theme-default pop-modal-dialog-content'
            });
        }
    }
})();