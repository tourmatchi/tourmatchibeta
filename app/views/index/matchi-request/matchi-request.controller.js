(function () {
    'use strict';

    angular.module('app.index').controller('MatchiRequestCtrl', MatchiRequestCtrl);

    MatchiRequestCtrl.$inject = ['MatchiRequests', '$window', 'Accounts'];
    function MatchiRequestCtrl(MatchiRequests, $window, Accounts) {
        var vm = this;
        vm.matchiRequestModel = {};
        vm.submit = submit;

        function submit() {
            console.log(vm.matchiRequestModel);
            MatchiRequests.matchiRequests(vm.matchiRequestModel, function (data) {
                console.log(data);
                alert('收到您的回覆了！會盡快為您找到適合的顧問再通知您！');
                vm.matchiRequestModel = {};
                window.location.replace("http://www.tourmatchi.com");
            }, function (error) {
                console.log(error);
                alert('請先登入！');
                Accounts.logout();
                $window.location.reload();
            });
        }
    }
})();