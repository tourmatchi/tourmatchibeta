(function () {
    'use strict';

    angular.module('app.index').controller('PopUpCtrl', PopUpCtrl);

    PopUpCtrl.$inject = ['Accounts', 'ngDialog', '$window'];

    function PopUpCtrl(Accounts, ngDialog, $window) {
        var vm = this;
        vm.register = register;
        vm.login = login;
        vm.model = {};
        vm.registerForm = {};
        vm.loginForm = {};
        vm.submitEnable = true;

        function register() {
            if (vm.registerForm.$valid) {
                vm.submitEnable = false;
                Accounts.register(vm.model, function () {
                    console.log('success');
                    vm.submitEnable = true;
                    alert('註冊成功！請去信箱收信啟用帳號');
                    ngDialog.close();
                }, function () {
                    console.log('error');
                    vm.submitEnable = true;
                    alert('帳號有誤');
                    vm.model = {};
                });
            }
        }

        function login() {
            if (vm.loginForm.$valid) {
                vm.submitEnable = false;

                Accounts.login(vm.model, function (data) {
                    vm.submitEnable = true;
                    alert('登入成功！');
                    console.log(data);
                    ngDialog.close();
                    $window.location.reload();
                }, function () {
                    vm.submitEnable = true;
                    alert('帳號已經註冊或是尚未啟用');
                    vm.model = {};
                });
            }
        }
    }
})();