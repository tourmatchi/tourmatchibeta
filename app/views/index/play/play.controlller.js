(function () {
    'use strict';

    angular.module('app.index').controller('PlayCtrl', PlayCtrl);

    PlayCtrl.$inject = ['Plays'];

    function PlayCtrl(Plays) {
        var vm = this;

        activate();

        function activate() {
            var plays = Plays.query();
            vm.plays = plays;
        }
    }
})();