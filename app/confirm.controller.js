(function () {
    'use strict';

    angular.module('app').controller('ConfirmCtrl', ConfirmCtrl);

    ConfirmCtrl.$inject = ['$routeParams', 'Accounts', '$location'];

    function ConfirmCtrl($routeParams, Accounts, $location) {
        var confirmToken = $routeParams.confirmToken;
        console.log(confirmToken, 'token');
        Accounts.confirm({'confirm_token': confirmToken}
            , function (data) {
                console.log(data, 'success');
                alert('帳號已啟用！');
                $location.path('/');
            }, function (error) {
                console.log(error, 'error');
                alert('啟用碼錯誤或是已經啟用！');
                $location.path('/');
            });
    }
})();