(function () {
    'use strict';

    angular.module('app.api').factory('Matchii', MatchiiFactory);

    MatchiiFactory.$inject = ['apiUrl', '$resource'];

    function MatchiiFactory(apiUrl, $resource) {
        var matchii = $resource(apiUrl('matchi/matchii'));
        return matchii;
    }
})();