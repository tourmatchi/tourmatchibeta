(function () {
    'use strict';

    angular.module('app.api').service('MatchiRequests', MatchiRequestsFactory);

    MatchiRequestsFactory.$inject = ['apiUrl', 'Auth'];

    function MatchiRequestsFactory(apiUrl, Auth) {
        return {
            matchiRequests: matchiRequests
        };

        function matchiRequests(matchiRequestModel, success, error) {
            $.ajax
            ({
                type: "POST",
                url: apiUrl("matchiRequest/matchiRequests"),
                dataType: 'json',
                async: false,
                headers: {
                    "Authorization": Auth.accessToken
                },
                data: matchiRequestModel,
                success: function (data) {
                    var auth = angular.fromJson(data);
                    Auth.update({
                        userId: auth.id,
                        email: auth.email,
                        accessToken: auth.access_token,
                        renewToken: auth.renew_token,
                        expireTime: auth.expire_time
                    });
                    success(data);
                }, error: function (data) {
                    error(data);
                }
            });
        }
    }
})();