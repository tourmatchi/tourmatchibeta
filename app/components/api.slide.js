(function () {
    'use strict';

    angular.module('app.api').factory('Slides', SlidesFactory);

    SlidesFactory.$inject = ['apiUrl', '$resource'];

    function SlidesFactory(apiUrl, $resource) {
        var slides = $resource(apiUrl('slide/slides'));
        return slides;
    }
})();