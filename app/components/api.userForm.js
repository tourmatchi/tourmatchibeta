(function () {
    'use strict';

    angular.module('app.api').service('UserForms', UserFormsFactory);

    UserFormsFactory.$inject = ['apiUrl', '$resource', 'Auth'];

    function UserFormsFactory(apiUrl, $resource, Auth) {
        var topics = $resource(apiUrl('topic/topics'));
        return {
            postUserForm: postUserForm,
            topics: topics
        };

        function postUserForm(userFormModel, success, error) {
            $.ajax
            ({
                type: "POST",
                url: apiUrl("userForm/userForms"),
                dataType: 'json',
                async: false,
                headers: {
                    "Authorization": Auth.accessToken
                },
                data: userFormModel,
                success: function (data) {
                    success(data);
                }, error: function (data) {
                    error(data);
                }
            });
        }
    }
})();