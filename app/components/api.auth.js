(function () {
    'use strict';

    angular.module('app.api')
        .factory('Auth', AuthFactory)
        .service('Cache', CacheService);

    AuthFactory.$inject = ['Cache'];
    function AuthFactory(Cache) {
        return Cache.bindLocalStorage('Auth', {
            userId: null,
            email: null,
            name: null,
            accessToken: null,
            renewToken: null,
            expireTime: null,
            isLogin: false
        });
    }

    CacheService.$inject = ['$localStorage'];
    function CacheService($localStorage) {
        function update(info) {
            var keys = Object.keys(info);
            return function (data) {
                keys.forEach(function (key) {
                    if (data[key])
                        info[key] = data[key];
                });
            };
        }

        function clean(info) {
            var keys = Object.keys(info);
            return function () {
                keys.forEach(function (key) {
                    info[key] = null;
                });
            };
        }

        this.bindLocalStorage = function (storageName, info) {
            angular.extend(info, {
                update: update(info),
                clean: clean(info)
            });

            // load data from local storage
            info.update($localStorage[storageName] || {});
            console.log(storageName, info);

            // info bind with local storage
            $localStorage[storageName] = info;

            return info;
        };
    }
})();