(function () {
    'use strict';

    angular.module('app').filter('second', second);

    function second() {
        return function (number) {
            return number + "s";
        }
    }
})();