(function () {
    'use strict';

    angular.module('app.api').factory('Plays', PlaysFactory);

    PlaysFactory.$inject = ['apiUrl', '$resource'];

    function PlaysFactory(apiUrl, $resource) {
        var plays = $resource(apiUrl('play/plays'));
        return plays;
    }
})();