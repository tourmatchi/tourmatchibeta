(function () {
    'use strict';

    angular.module('app.api').factory('Services', ServicesFactory);

    ServicesFactory.$inject = ['apiUrl', '$resource'];

    function ServicesFactory(apiUrl, $resource) {
        var services = $resource(apiUrl('service/services'));
        return services;
    }
})();