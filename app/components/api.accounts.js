(function () {
    'use strict';

    angular.module('app.api').factory('Accounts', AccountsFactory);

    AccountsFactory.$inject = ['apiUrl', 'Auth'];

    function AccountsFactory(apiUrl, Auth) {
        return {
            login: login,
            confirm: confirm,
            register: register,
            logout: logout
        };

        function logout() {
            Auth.clean();
        }

        function login(loginModel, success, error) {
            $.ajax
            ({
                type: "POST",
                url: apiUrl("auth/login"),
                dataType: 'json',
                async: false,
                data: loginModel,
                success: function (data) {
                    console.log("after login", data);
                    Auth.update({
                        userId: data.id,
                        email: data.email,
                        name: data.name,
                        accessToken: data.access_token,
                        renewToken: data.renew_token,
                        expireTime: data.expire_time,
                        isLogin: true
                    });
                    success(data);
                }, error: function (data) {
                    error(data);
                }
            });
        }

        function confirm(confirmModel, success, error) {
            $.ajax
            ({
                type: "GET",
                url: apiUrl("account/confirm"),
                dataType: 'json',
                async: false,
                data: confirmModel,
                success: function (data) {
                    success(data);
                }, error: function (data) {
                    error(data);
                }
            });
        }

        function register(registerModel, success, error) {
            $.ajax
            ({
                type: "POST",
                url: apiUrl("account/register"),
                dataType: 'json',
                async: false,
                //headers: {
                //    "Authorization": "Basic "
                //},
                data: registerModel,
                success: function (data) {
                    success(data);
                }, error: function (data) {
                    error(data);
                }
            });
        }
    }
})();